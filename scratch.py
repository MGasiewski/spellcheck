
def alignment(adj_matrix, source, destination):
    dest_align = ""
    source_align = ""

def min_edit_dist(source, destination):
    dest_align = ""
    source_align = ""
    edit_dist_matrix = [[0 for x in range(len(source) + 1)] for x in range(len(destination) + 1)]
    adj_matrix = [["" for x in range(len(source) + 1)] for x in range(len(destination) + 1)]
    for i in range(len(destination) + 1):
        edit_dist_matrix[i][0] = i
        adj_matrix[i][0] = "d"
    for j in range(len(source) + 1):
        edit_dist_matrix[0][j] = j
        adj_matrix[0][j] = "r"

    adj_matrix[0][0] = "o"
        
    for i in range(1, len(destination) + 1):
        for j in range(1, len(source) + 1):
            edit_dist_matrix[i][j] = min(edit_dist_matrix[i-1][j] + 1, edit_dist_matrix[i][j-1] + 1, edit_dist_matrix[i-1][j-1] if destination[i-1] == source[j-1] else edit_dist_matrix[i-1][j-1] + 2)
            if edit_dist_matrix[i][j] == edit_dist_matrix[i-1][j] + 1:
                adj_matrix[i][j] += "d"
            if edit_dist_matrix[i][j] == edit_dist_matrix[i][j-1] + 1:
                adj_matrix[i][j] += "r"
            if edit_dist_matrix[i][j] == edit_dist_matrix[i-1][j-1] + 2:
                adj_matrix[i][j] += "s"
            if destination[i-1] == source[j-1]:
                adj_matrix[i][j] += "="

    x = len(destination)
    y = len(source)
    z = ""
    while z != "o":
        if "=" in adj_matrix[x][y]:
            dest_align = destination[x-1] + dest_align
            source_align = source[y-1] + source_align
            x -= 1
            y -= 1
        elif "s" in adj_matrix[x][y]:
            dest_align = destination[x-1] + dest_align
            source_align = source[y-1] + source_align
            x -= 1
            y -= 1
        elif "d" in adj_matrix[x][y]:
            dest_align = destination[x-1] + dest_align
            source_align = "-" + source_align
            x -= 1
        else:
            dest_align = "-" + dest_align
            source_align = source[y-1] + source_align
            y -= 1
        z = adj_matrix[x][y]

    for i in edit_dist_matrix:
        print(i)

    for i in adj_matrix:
        print(i)

    return edit_dist_matrix[len(destination)][len(source)], source_align, dest_align

print(min_edit_dist("bonerfy", "bonerific"))