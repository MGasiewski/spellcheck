from nltk import *
from re import *


class SpellChecker(object):

    shakespeare = []
    scrabble = []
    f = open("output.txt", "w")

    def __init__(self):
        self.scrabble = open("wordlist.txt").read().split()
        self.scrabble = [s.lower() for s in self.scrabble]
        self.shakespeare = tokenize.word_tokenize(open("shakespeare.txt").read())
        self.shakespeare = set([s.lower() for s in self.shakespeare if re.match(r"[A-z]+", s) or re.match(r"[A-z]+'[A-z]+", s) or re.match(r"[A-z]+-[A-z]+", s)])

    def alignment(self, adj_matrix, source, destination):
        dest_align = ""
        source_align = ""
        x = len(destination)
        y = len(source)
        z = ""
        while z != "o":
            if "=" in adj_matrix[x][y]:
                dest_align = destination[x-1] + dest_align
                source_align = source[y-1] + source_align
                x -= 1
                y -= 1
            elif "s" in adj_matrix[x][y]:
                dest_align = destination[x-1] + dest_align
                source_align = source[y-1] + source_align
                x -= 1
                y -= 1
            elif "d" in adj_matrix[x][y]:
                dest_align = destination[x-1] + dest_align
                source_align = "-" + source_align
                x -= 1
            else:
                dest_align = "-" + dest_align
                source_align = source[y-1] + source_align
                y -= 1
            z = adj_matrix[x][y]
        return source_align, dest_align

    def min_edit_dist(self, source, destination):
        adj_matrix = [["" for x in range(len(source) + 1)] for x in range(len(destination) + 1)]
        edit_dist_matrix = [[0 for x in range(len(source) + 1)] for x in range(len(destination) + 1)]
        for i in range(len(destination) + 1):
            edit_dist_matrix[i][0] = i
            adj_matrix[i][0] = "d"
        for j in range(len(source) + 1):
            edit_dist_matrix[0][j] = j
            adj_matrix[0][j] = "r"

        adj_matrix[0][0] = "o"

        for i in range(1, len(destination) + 1):
            for j in range(1, len(source) + 1):
                edit_dist_matrix[i][j] = min(edit_dist_matrix[i-1][j] + 1, edit_dist_matrix[i][j-1] + 1, edit_dist_matrix[i-1][j-1] if destination[i-1] == source[j-1] else edit_dist_matrix[i-1][j-1] + 2)
                if edit_dist_matrix[i][j] == edit_dist_matrix[i-1][j] + 1:
                    adj_matrix[i][j] += "d"
                if edit_dist_matrix[i][j] == edit_dist_matrix[i][j-1] + 1:
                    adj_matrix[i][j] += "r"
                if edit_dist_matrix[i][j] == edit_dist_matrix[i-1][j-1] + 2:
                    adj_matrix[i][j] += "s"
                if destination[i-1] == source[j-1]:
                    adj_matrix[i][j] += "="

        source_align, dest_align = self.alignment(adj_matrix, source, destination)
        return edit_dist_matrix[len(destination)][len(source)], source_align, dest_align

    def check_word(self, word, dictionary="sh"):
        self.f.write("MISSPELLED WORD: " + word.upper() + "\n")
        candidates = []
        one_edit_candidates = []
        count = 1
        if dictionary == "sh":
            for entry in self.shakespeare:
                if self.min_edit_dist(word, entry)[0] < 3:
                    candidates.append((entry, self.min_edit_dist(word, entry)))
        else:
            for entry in self.scrabble:
                if self.min_edit_dist(word, entry)[0] < 3:
                    candidates.append((entry, self.min_edit_dist(word, entry)))
        one_edit_candidates = [c for c in candidates if c[1][0] == 1]
        same_letter_candidates = [c for c in candidates if c[1][0] == 2 and c[0][0] == word[0]]
        two_edit_candidates = [c for c in candidates if c[1][0] >= 2 and c[0][0] != word[0]]
        three_edit_candidates = [c for c in candidates if c[1][0] == 3]
        while count < 4:
            if len(one_edit_candidates) > 0:
                candidate = one_edit_candidates.pop()
            elif len(same_letter_candidates) > 0:
                candidate = same_letter_candidates.pop()
            elif len(two_edit_candidates) > 0:
                candidate = two_edit_candidates.pop()
            elif len(three_edit_candidates) > 0:
                candidate = three_edit_candidates.pop()
            else:
                count = 4
                break
            self.f.write("choice " + str(count) + "\n")
            self.f.write("candidate: " + str(candidate[0]) + "\n")
            self.f.write("edit distance: " + str(candidate[1][0]) + "\n")
            self.f.write("misspelled word alignment: " + str(candidate[1][1]) + "\n")
            self.f.write("corrected  word alignment: " + str(candidate[1][2]) + "\n")
            self.f.write("---------------------------------------------------------\n")
            count += 1

    def check_text(self, text):
        text = tokenize.word_tokenize(text)
        text = [w.lower() for w in text if re.match(r"[A-z]+", w) or re.match(r"[A-z]+'[A-z]+", w) or re.match(r"[A-z]+-[A-z]+", w)]
        for word in text:
            word = word.lower()
            if word not in self.shakespeare:
                self.check_word(word)
        self.f.write("SCRABBLE CHECK")
        for word in text:
            word = word.lower()
            if word not in self.scrabble:
                self.check_word(word, "scrabble")

    def end_program(self):
        self.f.close()

sp = SpellChecker()
sp.end_program()