from nltk import tokenize
import re


class SpellChecker(object):
    """A class that implements a variety of functions to check a text's spelling
    & output to an output file. I encapsulated these functions in a class
    because several of the methods need to use the same data, namely
    the shakespeare dictionary and the scrabble dictionary,
    but also the output file."""

    shakespeare = []
    scrabble = []
    output_file = open("output2.txt", "w")

    def __init__(self):
        """initializes the SpellChecker object. Reads the text files for words
        to be used in spell-checking dictionaries and tokenizes them"""
        self.scrabble = open("wordlist.txt").read().split()
        self.scrabble = [s.lower() for s in self.scrabble]
        self.shakespeare = tokenize.word_tokenize(open("shakespeare.txt").read())
        self.shakespeare = set([s.lower() for s in self.shakespeare
                                if re.match(r"[A-z]+", s)
                                or re.match(r"[A-z]+'[A-z]+", s)
                                or re.match(r"[A-z]+-[A-z]+", s)])

    def alignment(self, adj_matrix, source, destination):
        """determines the alignment of two string given the adjacency matrix
        provided"""
        dest_align = ""
        source_align = ""
        dest_pos = len(destination)
        source_pos = len(source)
        current_pos = ""
        while current_pos != "o":
            if "=" in adj_matrix[dest_pos][source_pos]:
                dest_align = destination[dest_pos-1] + dest_align
                source_align = source[source_pos-1] + source_align
                dest_pos -= 1
                source_pos -= 1
            elif "s" in adj_matrix[dest_pos][source_pos]:
                dest_align = destination[dest_pos-1] + dest_align
                source_align = source[source_pos-1] + source_align
                dest_pos -= 1
                source_pos -= 1
            elif "d" in adj_matrix[dest_pos][source_pos]:
                dest_align = destination[dest_pos-1] + dest_align
                source_align = "-" + source_align
                dest_pos -= 1
            else:
                dest_align = "-" + dest_align
                source_align = source[source_pos-1] + source_align
                source_pos -= 1
            current_pos = adj_matrix[dest_pos][source_pos]
        return source_align, dest_align

    def min_edit_dist(self, source, destination):
        """finds the minimum edit distance given two words. Also passes
         an adjacency matrix to the alignment function"""
        adj_matrix = [["" for x in range(len(source) + 1)]
                      for x in range(len(destination) + 1)]
        edit_dist_matrix = [[0 for x in range(len(source) + 1)]
                            for x in range(len(destination) + 1)]
        for i in range(len(destination) + 1):
            edit_dist_matrix[i][0] = i
            adj_matrix[i][0] = "d"
        for j in range(len(source) + 1):
            edit_dist_matrix[0][j] = j
            adj_matrix[0][j] = "r"

        adj_matrix[0][0] = "o"

        for i in range(1, len(destination) + 1):
            for j in range(1, len(source) + 1):
                edit_dist_matrix[i][j] = min(edit_dist_matrix[i-1][j] + 1,
                                             edit_dist_matrix[i][j-1] + 1,
                                             edit_dist_matrix[i-1][j-1]
                                             if destination[i-1] == source[j-1]
                                             else edit_dist_matrix[i-1][j-1] + 2)
                if edit_dist_matrix[i][j] == edit_dist_matrix[i-1][j] + 1:
                    adj_matrix[i][j] += "d"
                if edit_dist_matrix[i][j] == edit_dist_matrix[i][j-1] + 1:
                    adj_matrix[i][j] += "r"
                if edit_dist_matrix[i][j] == edit_dist_matrix[i-1][j-1] + 2:
                    adj_matrix[i][j] += "s"
                if destination[i-1] == source[j-1]:
                    adj_matrix[i][j] += "="

        source_align, dest_align = self.alignment(adj_matrix, source, destination)
        return edit_dist_matrix[len(destination)][len(source)], source_align, dest_align

    def check_word(self, word, dictionary="sh"):
        """checks an individual word's correctness"""
        self.output_file.write("MISSPELLED WORD: " + word.upper() + "\n")
        candidates = []
        one_edit_candidates = []
        count = 1
        if dictionary == "sh":
            for entry in self.shakespeare:
                if self.min_edit_dist(word, entry)[0] < 3:
                    candidates.append((entry, self.min_edit_dist(word, entry)))
        else:
            for entry in self.scrabble:
                if self.min_edit_dist(word, entry)[0] < 3:
                    candidates.append((entry, self.min_edit_dist(word, entry)))
        one_edit_candidates = [c for c in candidates if c[1][0] == 1]
        same_letter_candidates = [c for c in candidates if c[1][0] == 2 and c[0][0] == word[0]]
        two_edit_candidates = [c for c in candidates if c[1][0] >= 2 and c[0][0] != word[0]]
        three_edit_candidates = [c for c in candidates if c[1][0] == 3]
        while count < 4:
            if len(one_edit_candidates) > 0:
                candidate = one_edit_candidates.pop()
            elif len(same_letter_candidates) > 0:
                candidate = same_letter_candidates.pop()
            elif len(two_edit_candidates) > 0:
                candidate = two_edit_candidates.pop()
            elif len(three_edit_candidates) > 0:
                candidate = three_edit_candidates.pop()
            else:
                count = 4
                break
            self.output_file.write("choice " + str(count) + "\n")
            self.output_file.write("candidate: " + str(candidate[0]) + "\n")
            self.output_file.write("edit distance: " + str(candidate[1][0]) + "\n")
            self.output_file.write("misspelled word alignment: " + str(candidate[1][1]) + "\n")
            self.output_file.write("corrected  word alignment: " + str(candidate[1][2]) + "\n")
            self.output_file.write("---------------------------------------------------------\n")
            count += 1

    def check_text(self, text):
        """checks the whole text's correctness by checking each word
        to see if it's in one of the dictionaries and if it's not,
        passing it to check_word"""
        text = tokenize.word_tokenize(text)
        sh_count = 0
        sc_count = 0
        text = [w.lower() for w in text if re.match(r"[A-z]+", w)
                or re.match(r"[A-z]+'[A-z]+", w)
                or re.match(r"[A-z]+-[A-z]+", w)]
        for word in text:
            print(word)
            word = word.lower()
            if word not in self.shakespeare:
                self.check_word(word)
                sh_count += 1
        self.output_file.write("\n NUMBER OF MISSPELLED WORDS ACCORDING TO THE SHAKESPEARE"
                               "DICTIONARY: " + str(sh_count) + "\n")
        self.output_file.write("\nSCRABBLE CHECK\n")
        for word in text:
            print(word)
            word = word.lower()
            if word not in self.scrabble:
                self.check_word(word, "scrabble")
                sc_count += 1
        self.output_file.write("\n NUMBER OF MISSPELLED WORDS ACCORDING TO THE SCRABBLE"
                               "DICTIONARY: " + str(sc_count) + "\n")

    def end_program(self):
        """provides a way to close the file once the spellchecker has finished with the text"""
        self.output_file.close()

spellcheck = SpellChecker()
spellcheck.check_text(open("test2.txt").read())
spellcheck.end_program()